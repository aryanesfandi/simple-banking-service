package com.simple.banking.repository;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.domain.CurrencyRate;
import com.simple.banking.domain.enums.Currency;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;

import javax.persistence.NoResultException;
import java.util.Optional;

@Singleton
@Slf4j
public class CurrencyRateRepository extends GenericRepository<CurrencyRate> {

    @Inject
    public CurrencyRateRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<CurrencyRate> getRateByCurrency(Currency source, Currency destination) {
        CurrencyRate result = null;
        try {
            String hql = "from CurrencyRate u where u.source = :source and u.destination= :destination";
            result = (CurrencyRate) currentSession().createQuery(hql)
                    .setParameter("source", source)
                    .setParameter("destination", destination)
                    .getSingleResult();
        } catch (NoResultException exception) {
            log.warn(String.format("No exchange rate found with [%s] and [%s]", source, destination));
        }
        return Optional.ofNullable(result);
    }
}
