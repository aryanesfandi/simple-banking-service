package com.simple.banking.repository;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.domain.Account;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;

import javax.persistence.NoResultException;
import java.util.Optional;

@Singleton
@Slf4j
public class AccountRepository extends GenericRepository<Account> {

    @Inject
    public AccountRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<Account> getAccountByIban(String iban) {
        Account result = null;
        try {
            String hql = "from Account u where u.iban = :iban and u.enabled= :enabled";
            result = (Account) currentSession().createQuery(hql)
                    .setParameter("iban", iban)
                    .setParameter("enabled", true)
                    .getSingleResult();
        } catch (NoResultException exception) {
            log.warn("No data found in get account by iban" + iban);
        }
        return Optional.ofNullable(result);

    }

}
