package com.simple.banking.repository;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.domain.Transaction;
import org.hibernate.SessionFactory;

@Singleton
public class TransactionRepository extends GenericRepository<Transaction> {
    @Inject
    public TransactionRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
