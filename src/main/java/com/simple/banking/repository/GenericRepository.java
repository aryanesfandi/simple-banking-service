package com.simple.banking.repository;

import com.querydsl.jpa.hibernate.HibernateQueryFactory;
import com.simple.banking.domain.BaseEntry;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

public abstract class GenericRepository<T extends BaseEntry> extends AbstractDAO<T> {

    private HibernateQueryFactory factory;

    protected GenericRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.factory = new HibernateQueryFactory(this::currentSession);
    }

    protected HibernateQueryFactory query() {
        return factory;
    }

    public T saveOrUpdate(T model) {
        model.setUpdated(new Date());
        return persist(model);
    }

    public void saveOrUpdate(Collection<T> models) {
        models.forEach(this::persist);
    }

    public T findById(UUID id) {
        return get(id);
    }

    public Optional<T> getById(UUID id) {
        return Optional.ofNullable(get(id));
    }

    public void delete(T object) {
        if (object != null) {
            currentSession().delete(object);
        }
    }

    public int delete(Collection<T> objects) {
        objects.forEach(this::delete);
        return objects.size();
    }

    public void flush() {
        currentSession().flush();
    }

    public void deleteById(Long id) {
        delete(get(id));
    }
}
