package com.simple.banking.repository;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.domain.User;
import org.hibernate.SessionFactory;

@Singleton
public class UserRepository extends GenericRepository<User> {
    @Inject
    public UserRepository(SessionFactory sessionFactory) {
        super(sessionFactory);
    }
}
