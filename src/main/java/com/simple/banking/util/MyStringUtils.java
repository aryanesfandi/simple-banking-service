package com.simple.banking.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@UtilityClass
public class MyStringUtils {

    public String toJson(Object object) {
        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String outputJson = null;
        try {
            outputJson = ow.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            log.error(e.toString());
        }
        return outputJson;
    }

    public double similarityDistance(String name1, String name2) {
        int maxTolerance = 1;
        if (name1.length() > 5)
            maxTolerance = 2;
        if (name1.length() > 10)
            maxTolerance = 3;
        if (Math.abs(name1.length() - name2.length()) > maxTolerance)
            return 0;
        name1 = normalizeName(name1);
        name2 = normalizeName(name2);
        return StringUtils.getJaroWinklerDistance(name1, name2);
    }

    private static String normalizeName(String value) {
        value = value.toUpperCase();
        return value.trim();
    }
}
