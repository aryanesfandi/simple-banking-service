package com.simple.banking.controller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.controller.contract.CurrencyRateApi;
import com.simple.banking.dto.CreateCurrencyRateDTO;
import com.simple.banking.dto.CurrencyRateDTO;
import com.simple.banking.service.CurrencyRateService;
import lombok.AllArgsConstructor;

import javax.validation.Valid;
import java.util.UUID;

@Singleton
@AllArgsConstructor(onConstructor = @__(@Inject))
public class CurrencyRateController implements CurrencyRateApi {
    private CurrencyRateService currencyRateService;

    @Override
    public CurrencyRateDTO get(UUID id) {
        return currencyRateService.getCurrencyRate(id);
    }

    @Override
    public CurrencyRateDTO create(@Valid CreateCurrencyRateDTO createCurrencyRateDTO) {
        return currencyRateService.saveCurrencyRate(createCurrencyRateDTO);
    }
}
