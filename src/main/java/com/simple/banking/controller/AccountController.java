package com.simple.banking.controller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.controller.contract.AccountApi;
import com.simple.banking.dto.AccountDTO;
import com.simple.banking.dto.CreateAccountDTO;
import com.simple.banking.service.AccountService;
import lombok.AllArgsConstructor;

import javax.validation.Valid;
import java.util.UUID;

@Singleton
@AllArgsConstructor(onConstructor = @__(@Inject))
public class AccountController implements AccountApi {
    private AccountService accountService;

    @Override
    public AccountDTO get(UUID id) {
        return accountService.getAccount(id);
    }

    @Override
    public AccountDTO create(@Valid CreateAccountDTO createAccountDTO) {
        return accountService.saveAccount(createAccountDTO);
    }
}
