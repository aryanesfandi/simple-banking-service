package com.simple.banking.controller.contract;

import com.simple.banking.dto.AccountDTO;
import com.simple.banking.dto.CreateAccountDTO;
import com.wordnik.swagger.annotations.ApiOperation;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface AccountApi {

    @GET
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation(value = "get account by account id")
    AccountDTO get(@PathParam("id") UUID id);

    @POST
    @UnitOfWork
    @ApiOperation(value = "create account")
    AccountDTO create(@Valid CreateAccountDTO createAccountDTO);
}
