package com.simple.banking.controller.contract;

import com.simple.banking.dto.CreateUserDTO;
import com.simple.banking.dto.UserDTO;
import com.wordnik.swagger.annotations.ApiOperation;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface UserApi {

    @GET
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation(value = "get user by user id")
    UserDTO get(@PathParam("id") UUID id);

    @POST
    @UnitOfWork
    @ApiOperation(value = "create user")
    UserDTO create(@Valid CreateUserDTO createUserDTO);
}
