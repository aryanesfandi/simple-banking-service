package com.simple.banking.controller.contract;

import com.simple.banking.dto.CreateTransactionDTO;
import com.simple.banking.dto.TransactionDTO;
import com.wordnik.swagger.annotations.ApiOperation;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface TransactionApi {

    @GET
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation(value = "get transaction by transaction id")
    TransactionDTO get(@PathParam("id") UUID id);

    @POST
    @ApiOperation(value = "create transaction")
    TransactionDTO create(@Valid CreateTransactionDTO person);
}
