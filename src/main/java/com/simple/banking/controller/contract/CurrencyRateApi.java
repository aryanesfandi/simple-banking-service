package com.simple.banking.controller.contract;

import com.simple.banking.dto.CreateCurrencyRateDTO;
import com.simple.banking.dto.CurrencyRateDTO;
import com.wordnik.swagger.annotations.ApiOperation;
import io.dropwizard.hibernate.UnitOfWork;

import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@Path("/currency")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface CurrencyRateApi {
    @GET
    @UnitOfWork
    @Path("/{id}")
    @ApiOperation(value = "get currency rate  by currency rate id")
    CurrencyRateDTO get(@PathParam("id") UUID id);

    @POST
    @UnitOfWork
    @ApiOperation(value = "create currency rate")
    CurrencyRateDTO create(@Valid CreateCurrencyRateDTO createCurrencyRateDTO);
}
