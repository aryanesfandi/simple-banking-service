package com.simple.banking.controller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.controller.contract.UserApi;
import com.simple.banking.dto.CreateUserDTO;
import com.simple.banking.dto.UserDTO;
import com.simple.banking.service.UserService;
import lombok.AllArgsConstructor;

import javax.validation.Valid;
import java.util.UUID;

@Singleton
@AllArgsConstructor(onConstructor = @__(@Inject))
public class UserController implements UserApi {
    private UserService userService;

    @Override
    public UserDTO get(UUID id) {
        return userService.getUser(id);
    }

    @Override
    public UserDTO create(@Valid CreateUserDTO createUserDTO) {
        return userService.saveUser(createUserDTO);
    }
}
