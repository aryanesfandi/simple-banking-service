package com.simple.banking.controller;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.controller.contract.TransactionApi;
import com.simple.banking.dto.CreateTransactionDTO;
import com.simple.banking.dto.TransactionDTO;
import com.simple.banking.repository.UnitOfWork;
import com.simple.banking.service.TransactionService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;
import java.util.UUID;

@Slf4j
@Singleton
@AllArgsConstructor(onConstructor = @__(@Inject))
public class TransactionController implements TransactionApi {
    private TransactionService transactionService;
    private final SessionFactory sessionFactory;
    private static final Integer NUMBER_OF_TRY = 10;

    @Override
    public TransactionDTO get(UUID id) {
        return transactionService.getTransaction(id);
    }

    @Override
    public TransactionDTO create(@Valid CreateTransactionDTO createTransactionDTO) {
        int retry = 0;
        OptimisticLockException optimisticLockException = new OptimisticLockException();

        while (retry < NUMBER_OF_TRY) {
            try {
                return UnitOfWork.call(sessionFactory, () -> transactionService.saveTransaction(createTransactionDTO));
            } catch (OptimisticLockException opt) {
                optimisticLockException = opt;
                log.warn(String.format("Concurrency exception problem for [%s] and [%s]",
                        createTransactionDTO.getSourceIban(), createTransactionDTO.getDestinationIban()));
                retry ++;
            }
        }

        throw optimisticLockException;
    }
}
