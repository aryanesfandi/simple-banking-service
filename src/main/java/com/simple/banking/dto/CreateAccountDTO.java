package com.simple.banking.dto;

import com.simple.banking.domain.enums.Currency;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateAccountDTO {

    @NotNull
    @ApiModelProperty(value = "Unique account human-readable id")
    private String accountNumber;

    @NotNull
    @ApiModelProperty(value = "branch id")
    private Integer branchId;

    @NotNull
    @ApiModelProperty(value = "international bank account number")
    private String iban;

    @NotNull
    @ApiModelProperty(value = "currency type", example = "USD")
    private Currency currency;

    @NotNull
    @ApiModelProperty(value = "owner user id")
    private UUID userId;
}
