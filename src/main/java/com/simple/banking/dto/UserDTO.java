package com.simple.banking.dto;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.simple.banking.domain.enums.UserType;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class UserDTO extends BaseEntityDTO {

    @NotNull
    @ApiModelProperty(value = "Unique user id")
    private UUID id;

    @NotNull
    @ApiModelProperty(value = "user type", example = "human")
    private UserType userType;

    @ApiModelProperty(value = "first name")
    private String firstName;

    @ApiModelProperty(value = "last name")
    private String lastName;

    @ApiModelProperty(value = "company name")
    private String companyName;

    @Email
    @ApiModelProperty(value = "email")
    private String email;

    @ApiModelProperty(value = "accounts")
    @JsonManagedReference
    private List<AccountDTO> accounts;
}
