package com.simple.banking.dto;

import com.simple.banking.domain.enums.Currency;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class TransactionDTO extends BaseEntityDTO {

    @NotNull
    @ApiModelProperty(value = "Unique transaction id")
    private UUID id;

    @NotNull
    @ApiModelProperty(value = "source international bank account number")
    private String sourceIban;

    @NotNull
    @ApiModelProperty(value = "destination international bank account number")
    private String destinationIban;

    @NotNull
    @ApiModelProperty(value = "currency", example = "GBP")
    private Currency currency;

    @NotNull
    @ApiModelProperty(value = "amount")
    private BigDecimal amount;

    @NotNull
    @ApiModelProperty(value = "transaction description")
    private String description;

    @ApiModelProperty(value = "accounts which involve in transaction")
    private Set<AccountDTO> accounts;
}
