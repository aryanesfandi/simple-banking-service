package com.simple.banking.dto;

import com.simple.banking.domain.enums.UserType;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateTransactionDTO {

    @NotNull
    @ApiModelProperty(value = "source international bank account number")
    private String sourceIban;

    @NotNull
    @ApiModelProperty(value = "destination international bank account number")
    private String destinationIban;

    @NotNull
    @ApiModelProperty(value = "amount")
    private BigDecimal amount;

    @NotNull
    @ApiModelProperty(value = "transaction description")
    private String description;

    @NotNull
    @ApiModelProperty(value = "destination user type", example = "human")
    private UserType destinationUserType;

    @ApiModelProperty(value = "destination first name")
    private String destinationFirstName;

    @ApiModelProperty(value = "destination last name")
    private String destinationLastName;

    @ApiModelProperty(value = "destination company name")
    private String destinationCompanyName;
}
