package com.simple.banking.dto;

import com.simple.banking.domain.enums.Currency;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateCurrencyRateDTO {

    @NotNull
    @ApiModelProperty(value = "source currency")
    private Currency source;

    @NotNull
    @ApiModelProperty(value = "destination currency")
    private Currency destination;

    @NotNull
    @ApiModelProperty(value = "exchange rate")
    private Double rate;
}
