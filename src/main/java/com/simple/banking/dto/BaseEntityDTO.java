package com.simple.banking.dto;

import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;


@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class BaseEntityDTO {
    @ApiModelProperty(value = "Time of creation")
    private Date created;

    @ApiModelProperty(value = "Time of update")
    private Date updated;
}
