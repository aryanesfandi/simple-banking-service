package com.simple.banking.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.simple.banking.domain.Transaction;
import com.simple.banking.domain.User;
import com.simple.banking.domain.enums.Currency;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;


@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class AccountDTO extends BaseEntityDTO {

    @NotNull
    @ApiModelProperty(value = "Unique account id")
    private UUID id;

    @NotNull
    @ApiModelProperty(value = "Unique account human-readable id")
    private String accountNumber;

    @NotNull
    @ApiModelProperty(value = "branch id")
    private Integer branchId;

    @NotNull
    @ApiModelProperty(value = "international bank account number")
    private String iban;

    @NotNull
    @ApiModelProperty(value = "currency type", example = "USD")
    private Currency currency;

    @NotNull
    @ApiModelProperty(value = "balance amount")
    private BigDecimal balance;

    @ApiModelProperty(value = "transactions history")
    @JsonBackReference
    private List<Transaction> history;


    @ApiModelProperty(value = "owner user")
    @JsonBackReference
    private User user;
}
