package com.simple.banking.dto;

import com.simple.banking.domain.enums.Currency;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CurrencyRateDTO extends BaseEntityDTO {

    @NotNull
    @ApiModelProperty(value = "Unique currency id")
    private UUID id;

    @NotNull
    @ApiModelProperty(value = "source currency")
    private Currency source;

    @NotNull
    @ApiModelProperty(value = "destination currency")
    private Currency destination;

    @NotNull
    @ApiModelProperty(value = "exchange rate")
    private double rate;
}
