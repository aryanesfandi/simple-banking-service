package com.simple.banking.dto;

import com.simple.banking.domain.enums.UserType;
import com.wordnik.swagger.annotations.ApiModelProperty;
import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateUserDTO {

    @NotNull
    @ApiModelProperty(value = "user type", example = "human")
    private UserType userType;

    @ApiModelProperty(value = "first name")
    private String firstName;

    @ApiModelProperty(value = "last name")
    private String lastName;

    @ApiModelProperty(value = "company name")
    private String companyName;

    @Email
    @ApiModelProperty(value = "email")
    private String email;
}
