package com.simple.banking;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.simple.banking.config.BankingConfiguration;
import com.simple.banking.config.BankingHealthCheck;
import com.simple.banking.config.BankingModule;
import com.simple.banking.controller.AccountController;
import com.simple.banking.controller.CurrencyRateController;
import com.simple.banking.controller.TransactionController;
import com.simple.banking.controller.UserController;
import com.simple.banking.domain.Account;
import com.simple.banking.domain.CurrencyRate;
import com.simple.banking.domain.Transaction;
import com.simple.banking.domain.User;
import com.simple.banking.exception.handler.*;
import com.wordnik.swagger.jaxrs.config.BeanConfig;
import com.wordnik.swagger.jaxrs.listing.ApiListingResource;
import com.wordnik.swagger.models.AbstractModel;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.server.DefaultServerFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.flywaydb.core.Flyway;
import ru.yandex.qatools.embed.postgresql.EmbeddedPostgres;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

import static org.apache.axis.utils.NetworkUtils.LOCALHOST;
import static ru.yandex.qatools.embed.postgresql.distribution.Version.Main.V9_6;

public class BankingApplication extends Application<BankingConfiguration> {

    private HibernateBundle<BankingConfiguration> hibernateBundle;
    private EmbeddedPostgres postgres;

    @Override
    public String getName() {
        return "BankingApplication";
    }


    @Override
    public void initialize(Bootstrap<BankingConfiguration> bootstrap) {
        List<Class> classes = Arrays.asList(User.class, Account.class, Transaction.class, CurrencyRate.class);

        hibernateBundle = new HibernateBundle<BankingConfiguration>(AbstractModel.class,
                classes.toArray(new Class[classes.size()])) {
            @Override
            public DataSourceFactory getDataSourceFactory(BankingConfiguration configuration) {
                if (configuration.getRunInMemoryDatabase())
                    runInMemoryDatabase(configuration.getDataSourceFactory());

                DataSourceFactory factory = configuration.getDataSourceFactory();
                Flyway flyway = Flyway.configure().dataSource(configuration.getDataSourceFactory().getUrl(), configuration.getDataSourceFactory().getUser(), configuration.getDataSourceFactory().getPassword()).load();
                flyway.migrate();
                return factory;
            }
        };


        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(new MigrationsBundle<BankingConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(BankingConfiguration configuration) {

                return configuration.getDataSourceFactory();
            }
        });

        bootstrap.addBundle(new AssetsBundle("/assets/", "/", "index.html"));
        bootstrap.addBundle(new MultiPartBundle());
    }


    @SneakyThrows
    private void runInMemoryDatabase(@Valid @NotNull DataSourceFactory dataSourceFactory) {
        postgres = new EmbeddedPostgres(V9_6);
        postgres.start(LOCALHOST,
                5432, "banking_app", dataSourceFactory.getUser(), dataSourceFactory.getPassword());
        Runtime.getRuntime().addShutdownHook(new Thread(() -> postgres.stop()));
    }

    @Override
    public void run(BankingConfiguration config, Environment environment) {
        // guice init
        BankingModule bankingModule = new BankingModule(hibernateBundle.getSessionFactory(), config, environment.metrics());
        Injector injector = Guice.createInjector(bankingModule);

        // configure context path
        environment.getApplicationContext().setContextPath(config.getContextPath());

        environment.jersey().register(new CustomerSimilarityExceptionHandler());


        // REST resources
        environment.jersey().setUrlPattern("/api/*");
        ((DefaultServerFactory) config.getServerFactory()).setJerseyRootPath("/api/*");
        environment.jersey().register(injector.getInstance(UserController.class));
        environment.jersey().register(injector.getInstance(AccountController.class));
        environment.jersey().register(injector.getInstance(TransactionController.class));
        environment.jersey().register(injector.getInstance(CurrencyRateController.class));
        environment.healthChecks().register("healthcheck", new BankingHealthCheck());

        environment.jersey().register(new CustomerSimilarityExceptionHandler());
        environment.jersey().register(new InSufficientBalanceExceptionHandler());
        environment.jersey().register(new LessThanMinimumExceptionHandler());
        environment.jersey().register(new NoCurrencyConverterExceptionHandler());
        environment.jersey().register(new ConcurrencyExceptionHandler());

        environment.jersey().register(new ApiListingResource());

        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);

        String modelsPackage = "com.simple.banking.dto";
        String contractsPackage = "com.simple.banking.controller.contract";
        List<String> packages = Arrays.asList(modelsPackage, contractsPackage);
        BeanConfig swaggerConfig = new BeanConfig();
        swaggerConfig.setTitle("Banking");
        swaggerConfig.setVersion("1");
        swaggerConfig.setBasePath("/api");
        swaggerConfig.setResourcePackage(StringUtils.join(packages, ","));
        swaggerConfig.setScan(true);
    }

    public static void main(String[] args) throws Exception {
        new BankingApplication().run(args);
    }
}
