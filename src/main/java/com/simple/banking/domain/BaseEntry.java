package com.simple.banking.domain;

import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.type.PostgresUUIDType;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.UUID;


@TypeDefs({
        @TypeDef(name = "pg-uuid", typeClass = PostgresUUIDType.class, defaultForType = UUID.class),
        @TypeDef(name = "pgsql-enum", typeClass = PostgreSQLEnumType.class),
})

@Data
@MappedSuperclass
public class BaseEntry {

    @Id
    @Type(type = "pg-uuid")
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private UUID id;

    @Column(name = "created")
    private Date created;

    @Column(name = "updated")
    private Date updated;

}
