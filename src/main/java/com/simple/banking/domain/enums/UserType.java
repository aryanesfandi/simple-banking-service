package com.simple.banking.domain.enums;

public enum UserType {
    PERSON,
    COMPANY
}