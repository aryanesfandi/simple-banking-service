package com.simple.banking.domain.enums;

public enum Currency {
    USD,
    EUR,
    GBP,
    PLN,
    RUB,
    UAH
}