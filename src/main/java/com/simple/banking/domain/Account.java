package com.simple.banking.domain;

import com.simple.banking.domain.enums.Currency;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Setter
@Getter
@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = "account")
public class Account extends BaseEntry {

    @Column(name = "account_number", nullable = false)
    private String accountNumber;

    @Column(name = "branch_id", nullable = false)
    private Integer branchId;

    @Column(name = "iban", nullable = false)
    private String iban;

    @Type(type = "pgsql-enum")
    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @Column(name = "balance", nullable = false)
    private BigDecimal balance;

    @ManyToMany(mappedBy = "accounts")
    private List<Transaction> history;


    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "enabled", nullable = false)
    private Boolean enabled = true;

    @Version
    @Column(name = "version", nullable = false)
    private Long version;
}
