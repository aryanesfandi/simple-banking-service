package com.simple.banking.domain;

import com.simple.banking.domain.enums.Currency;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@Table(name = "currency_rate")
public class CurrencyRate extends BaseEntry {

    @Type(type = "pgsql-enum")
    @Enumerated(EnumType.STRING)
    @Column(name = "source", nullable = false)
    private Currency source;

    @Type(type = "pgsql-enum")
    @Enumerated(EnumType.STRING)
    @Column(name = "destination", nullable = false)
    private Currency destination;

    @NotNull
    @Column(name = "rate", nullable = false)
    private double rate;
}
