package com.simple.banking.domain;

import com.simple.banking.domain.enums.Currency;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "transaction")
@EqualsAndHashCode(callSuper = true)
public class Transaction extends BaseEntry {

    @Column(name = "source_iban", nullable = false)
    private String sourceIban;

    @Column(name = "destination_iban", nullable = false)
    private String destinationIban;

    @Type(type = "pgsql-enum")
    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "description")
    private String description;

    @ManyToMany
    @JoinTable(name = "transaction_account",
            joinColumns = @JoinColumn(name = "transaction_id"),
            inverseJoinColumns = @JoinColumn(name = "account_id"))
    private Set<Account> accounts;
}
