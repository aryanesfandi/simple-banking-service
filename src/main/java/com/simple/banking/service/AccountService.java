package com.simple.banking.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.domain.Account;
import com.simple.banking.domain.User;
import com.simple.banking.dto.AccountDTO;
import com.simple.banking.dto.CreateAccountDTO;
import com.simple.banking.repository.AccountRepository;
import com.simple.banking.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;

import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.UUID;

@Slf4j
@Singleton
@AllArgsConstructor(onConstructor = @__(@Inject))
public class AccountService {
    private AccountRepository accountRepository;
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    public AccountDTO getAccount(UUID id) {
        log.info("get account by id  {}", id);

        return accountRepository.getById(id)
                .map(this::convert)
                .orElseThrow(() -> new NotFoundException("no account found"));
    }

    public AccountDTO saveAccount(CreateAccountDTO createCurrencyRateDTO) {
        log.info("save account dto  {}", createCurrencyRateDTO);

        User user = userRepository.getById(createCurrencyRateDTO.getUserId())
                .orElseThrow(() -> new NotFoundException("no user found"));

        Account inputAccount = convert(createCurrencyRateDTO);
        inputAccount.setUser(user);
        inputAccount.setBalance(BigDecimal.ZERO);
        Account account = accountRepository.saveOrUpdate(inputAccount);

        log.info("save account model {}", account);
        return convert(account);
    }

    private AccountDTO convert(Account account) {
        return modelMapper.map(account, AccountDTO.class);
    }

    private Account convert(CreateAccountDTO createAccountDTO) {
        return modelMapper.map(createAccountDTO, Account.class);
    }

}
