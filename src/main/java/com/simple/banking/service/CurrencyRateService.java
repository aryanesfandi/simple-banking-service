package com.simple.banking.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.domain.CurrencyRate;
import com.simple.banking.dto.CreateCurrencyRateDTO;
import com.simple.banking.dto.CurrencyRateDTO;
import com.simple.banking.repository.CurrencyRateRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;

import javax.ws.rs.NotFoundException;
import java.util.UUID;

@Slf4j
@Singleton
@AllArgsConstructor(onConstructor = @__(@Inject))
public class CurrencyRateService {
    private CurrencyRateRepository currencyRateRepository;
    private ModelMapper modelMapper;

    public CurrencyRateDTO getCurrencyRate(UUID id) {
        log.info("get currency rate {}", id);

        return currencyRateRepository.getById(id)
                .map(this::convert)
                .orElseThrow(() -> new NotFoundException("No converter found"));
    }

    public CurrencyRateDTO saveCurrencyRate(CreateCurrencyRateDTO createCurrencyRateDTO) {
        log.info("save currency rate {}", createCurrencyRateDTO);

        CurrencyRate currencyRate = currencyRateRepository.saveOrUpdate(convert(createCurrencyRateDTO));
        return convert(currencyRate);
    }

    private CurrencyRateDTO convert(CurrencyRate currencyRate) {
        return modelMapper.map(currencyRate, CurrencyRateDTO.class);
    }

    private CurrencyRate convert(CreateCurrencyRateDTO createCurrencyRateDTO) {
        return modelMapper.map(createCurrencyRateDTO, CurrencyRate.class);
    }

}
