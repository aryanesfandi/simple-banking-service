package com.simple.banking.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.domain.Account;
import com.simple.banking.domain.CurrencyRate;
import com.simple.banking.domain.Transaction;
import com.simple.banking.domain.User;
import com.simple.banking.domain.enums.Currency;
import com.simple.banking.domain.enums.UserType;
import com.simple.banking.dto.CreateTransactionDTO;
import com.simple.banking.dto.TransactionDTO;
import com.simple.banking.exception.CustomerSimilarityException;
import com.simple.banking.exception.InsufficientBalanceException;
import com.simple.banking.exception.LessThanMinimumException;
import com.simple.banking.exception.NoCurrencyConverterException;
import com.simple.banking.repository.AccountRepository;
import com.simple.banking.repository.CurrencyRateRepository;
import com.simple.banking.repository.TransactionRepository;
import com.simple.banking.util.MyStringUtils;
import io.dropwizard.hibernate.UnitOfWork;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;

import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;
import java.util.stream.Stream;

@Slf4j
@Singleton
@AllArgsConstructor(onConstructor = @__(@Inject))
public class TransactionService {
    private static final BigDecimal MINIMUM_AMOUNT_TO_TRANSFER = new BigDecimal(1);
    private static final double MINIMUM_ACCEPTANCE_SIMILARITY = 0.9;
    private TransactionRepository transactionRepository;
    private AccountRepository accountRepository;
    private CurrencyRateRepository currencyRateRepository;
    private ModelMapper modelMapper;

    public TransactionDTO getTransaction(UUID id) {
        log.info("get transaction by id  {}", id);
        return transactionRepository.getById(id)
                .map(this::convert)
                .orElseThrow(() -> new NotFoundException("No transaction found"));
    }

    @UnitOfWork
    public TransactionDTO saveTransaction(CreateTransactionDTO createTransactionDTO) {
        log.info("save transaction dto  {}", createTransactionDTO);

        Account source = accountRepository.getAccountByIban(createTransactionDTO.getSourceIban())
                .orElseThrow(
                        () -> new NotFoundException(String.format("account with [%s] iban not found", createTransactionDTO.getSourceIban())));

        Account destination = accountRepository.getAccountByIban(createTransactionDTO.getDestinationIban())
                .orElseThrow(
                        () -> new NotFoundException(String.format("account with [%s] iban not found", createTransactionDTO.getDescription())));


        checkIdentification(createTransactionDTO, destination);

        BigDecimal finalAmount = calculateFinalAmount(createTransactionDTO.getAmount(), source.getCurrency(), destination.getCurrency());
        transferMoney(finalAmount, createTransactionDTO.getAmount(), source, destination);

        Transaction finalTransaction = convert(createTransactionDTO);

        finalTransaction.setAccounts(new HashSet<>(Arrays.asList(source, destination)));
        finalTransaction.setCurrency(destination.getCurrency());
        finalTransaction.setAmount(finalAmount);

        Transaction transaction = transactionRepository.saveOrUpdate(finalTransaction);

        log.info("saved transaction model  {}", transaction);
        return convert(transaction);
    }

    private void checkIdentification(CreateTransactionDTO createTransactionDTO, Account destination) {
        User user = destination.getUser();
        if (UserType.PERSON.equals(createTransactionDTO.getDestinationUserType())) {
            if (checkSimilarityName(user.getFirstName(), createTransactionDTO.getDestinationFirstName()) ||
                    checkSimilarityName(user.getLastName(), createTransactionDTO.getDestinationLastName()))
                throw new CustomerSimilarityException();
        } else {
            if (checkSimilarityName(user.getCompanyName(), createTransactionDTO.getDestinationCompanyName()))
                throw new CustomerSimilarityException();
        }
    }

    private void transferMoney(BigDecimal finalAmount, BigDecimal sourceAmount, Account source, Account destination) {
        if (source.getBalance().compareTo(finalAmount) < 1)
            throw new InsufficientBalanceException();

        source.setBalance(source.getBalance().subtract(sourceAmount));
        destination.setBalance(destination.getBalance().add(finalAmount));

        Stream.of(source, destination)
                .forEach(accountRepository::saveOrUpdate);
    }

    private BigDecimal calculateFinalAmount(BigDecimal amount, Currency source, Currency destination) {
        if (MINIMUM_AMOUNT_TO_TRANSFER.compareTo(amount) > -1)
            throw new LessThanMinimumException();

        return currencyRateRepository.getRateByCurrency(source, destination)
                .map(CurrencyRate::getRate)
                .map(BigDecimal::new)
                .map(bigDecimal -> bigDecimal.multiply(amount))
                .orElseThrow(NoCurrencyConverterException::new);
    }

    private TransactionDTO convert(Transaction transaction) {
        return modelMapper.map(transaction, TransactionDTO.class);
    }

    private Transaction convert(CreateTransactionDTO createTransactionDTO) {
        return modelMapper.map(createTransactionDTO, Transaction.class);
    }

    private boolean checkSimilarityName(String name1, String name2) {
        return MyStringUtils.similarityDistance(name1, name2) < MINIMUM_ACCEPTANCE_SIMILARITY;
    }

}
