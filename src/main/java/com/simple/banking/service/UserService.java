package com.simple.banking.service;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.simple.banking.domain.User;
import com.simple.banking.dto.CreateUserDTO;
import com.simple.banking.dto.UserDTO;
import com.simple.banking.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;

import javax.ws.rs.NotFoundException;
import java.util.UUID;

@Slf4j
@Singleton
@AllArgsConstructor(onConstructor = @__(@Inject))
public class UserService {
    private UserRepository userRepository;
    private ModelMapper modelMapper;

    public UserDTO getUser(UUID id) {
        log.info("get user by id  {}", id);
        return userRepository.getById(id)
                .map(this::convert)
                .orElseThrow(() -> new NotFoundException("No User found"));
    }

    public UserDTO saveUser(CreateUserDTO createUserDTO) {
        log.info("save user dto  {}", createUserDTO);

        User user = userRepository.saveOrUpdate(convert(createUserDTO));

        log.info("save user model  {}", user);
        return convert(user);
    }

    private UserDTO convert(User user) {
        return modelMapper.map(user, UserDTO.class);
    }

    private User convert(CreateUserDTO createUserDTO) {
        return modelMapper.map(createUserDTO, User.class);
    }

}
