package com.simple.banking.exception.handler;

import com.simple.banking.exception.LessThanMinimumException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class LessThanMinimumExceptionHandler implements ExceptionMapper<LessThanMinimumException> {
    @Override
    public Response toResponse(LessThanMinimumException exception) {
        return Response.status(314).entity("Your transaction amount is less than minimum").type("text/plain").build();
    }
}
