package com.simple.banking.exception.handler;

import com.simple.banking.exception.InsufficientBalanceException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class InSufficientBalanceExceptionHandler implements ExceptionMapper<InsufficientBalanceException> {
    @Override
    public Response toResponse(InsufficientBalanceException exception) {
        return Response.status(312).entity("Your balance is not enough for this transaction").type("text/plain").build();
    }
}
