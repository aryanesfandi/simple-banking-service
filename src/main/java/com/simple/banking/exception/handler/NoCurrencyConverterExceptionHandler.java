package com.simple.banking.exception.handler;

import com.simple.banking.exception.NoCurrencyConverterException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NoCurrencyConverterExceptionHandler implements ExceptionMapper<NoCurrencyConverterException> {
    @Override
    public Response toResponse(NoCurrencyConverterException exception) {
        return Response.status(315).entity("No currency converter to exchange").type("text/plain").build();
    }
}
