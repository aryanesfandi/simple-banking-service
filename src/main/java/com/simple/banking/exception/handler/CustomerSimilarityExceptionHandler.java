package com.simple.banking.exception.handler;

import com.simple.banking.exception.CustomerSimilarityException;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class CustomerSimilarityExceptionHandler implements ExceptionMapper<CustomerSimilarityException> {
    @Override
    public Response toResponse(CustomerSimilarityException exception) {
        return Response.status(311).entity("Your target identification information is not correct").type("text/plain").build();
    }
}
