package com.simple.banking.config;

import com.codahale.metrics.MetricRegistry;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;

@RequiredArgsConstructor
@Slf4j
public class BankingModule extends AbstractModule {

    @Getter(onMethod = @__({@Provides}))
    private final SessionFactory sessionFactory;

    @Getter(onMethod = @__({@Provides}))
    private final BankingConfiguration config;

    @Getter(onMethod = @__({@Provides}))
    private final MetricRegistry metrics;

    @Override
    protected void configure() {
        //add custom config in future if needed
    }
}
