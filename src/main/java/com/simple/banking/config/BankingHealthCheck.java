package com.simple.banking.config;

import com.codahale.metrics.health.HealthCheck;

public class BankingHealthCheck extends HealthCheck {

    @Override
    protected Result check() throws Exception {
        return Result.healthy();
    }
}
