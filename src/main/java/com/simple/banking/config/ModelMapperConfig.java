package com.simple.banking.config;

import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.modelmapper.ModelMapper;
import org.modelmapper.config.Configuration;

@Singleton
public class ModelMapperConfig {
    @Provides
    public ModelMapper createModelMapper() {
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setFieldMatchingEnabled(true)
                .setFieldAccessLevel(Configuration.AccessLevel.PRIVATE);
        return modelMapper;
    }
}
