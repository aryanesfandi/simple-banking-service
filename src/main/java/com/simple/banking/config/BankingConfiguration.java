package com.simple.banking.config;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;
import lombok.Getter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.ResourceBundle;

@Getter
public class BankingConfiguration extends Configuration {
    private ResourceBundle bundle;

    public BankingConfiguration() {
        bundle = ResourceBundle.getBundle("application");
    }

    @Valid
    @NotNull
    @JsonProperty("database")
    private DataSourceFactory dataSourceFactory = new DataSourceFactory();

    private Boolean runInMemoryDatabase = true;

    @NotNull
    private String contextPath;


    public String getVersion() {
        return bundle.getString("version");
    }

    public String getGitCommit() {
        return bundle.getString("git.commit");
    }

}
