create schema IF NOT EXISTS banking_app;

SET search_path = banking_app;

CREATE TYPE banking_app.CurrencyType AS ENUM ('USD', 'EUR', 'GBP', 'PLN', 'RUB', 'UAH');

CREATE TYPE banking_app.UserType AS ENUM ('PERSON', 'COMPANY');

CREATE TABLE normal_user (
    id               uuid    NOT NULL PRIMARY KEY,
    user_type        UserType NOT NULL,
    first_name       TEXT,
    last_name        TEXT,
    company_name     TEXT,
    email            TEXT   NOT NULL,
    created          TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated          TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()
);

CREATE TABLE account (
    id               uuid    NOT NULL PRIMARY KEY,
    account_number   TEXT    NOT NULL,
    branch_id        INTEGER NOT NULL,
    iban             TEXT    NOT NULL,
    currency         CurrencyType  NOT NULL,
    balance          DECIMAL NOT NULL,
    user_id          uuid   REFERENCES normal_user(id),
    enabled          BOOLEAN NOT NULL DEFAULT TRUE,
    version          BIGINT  NOT NULL,
    created          TIMESTAMP WITHOUT TIME ZONE  DEFAULT NOW(),
    updated          TIMESTAMP WITHOUT TIME ZONE  DEFAULT NOW()
);

CREATE TABLE transaction (
    id               uuid    NOT NULL PRIMARY KEY,
    source_iban      TEXT    NOT NULL,
    destination_iban TEXT    NOT NULL,
    currency         CurrencyType  NOT NULL,
    amount           DECIMAL NOT NULL,
    description      TEXT,
    created          TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated          TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()
);


CREATE TABLE currency_rate (
    id               uuid    NOT NULL PRIMARY KEY,
    source           CurrencyType  NOT NULL,
    destination      CurrencyType  NOT NULL,
    rate             double precision NOT NULL,
    created          TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW(),
    updated          TIMESTAMP WITHOUT TIME ZONE DEFAULT NOW()
);

CREATE TABLE transaction_account (
    transaction_id       uuid    NOT NULL REFERENCES transaction(id),
    account_id  uuid    NOT NULL REFERENCES account(id)
);



