SET search_path = banking_app;

INSERT INTO normal_user(id, user_type, first_name, last_name, email)
VALUES('6dc7a8c0-4de6-11ea-b77f-2e728ce88125', 'PERSON','aryan', 'esfandi kashi', 'aryan@gmail.com');

INSERT INTO normal_user(id, user_type, first_name, last_name, email)
VALUES('6dc7ab68-4de6-11ea-b77f-2e728ce88125', 'PERSON','iman', 'yarandi', 'iman@gmail.com');

INSERT INTO account(id, account_number, branch_id, iban, currency, balance, user_id, version)
VALUES('8c2105f0-4de6-11ea-b77f-2e728ce88125', '63082894', 101, 'GB76MIDL07009363082894', 'EUR','500.0', '6dc7a8c0-4de6-11ea-b77f-2e728ce88125', 1);

INSERT INTO account(id, account_number, branch_id, iban, currency, balance, user_id, version)
VALUES('e534b384-4de7-11ea-b77f-2e728ce88125', '40930750', 102, 'GB47MIDL07009340930750', 'GBP','0.0', '6dc7ab68-4de6-11ea-b77f-2e728ce88125', 1);

INSERT INTO currency_rate(id, source, destination, rate)
VALUES('8c21037a-4de6-11ea-b77f-2e728ce88125', 'EUR','GBP', 0.84);

INSERT INTO transaction(id, source_iban, destination_iban, currency, amount, description)
VALUES('6dc7a8c0-4de6-11ea-b77f-2e728ce88125', 'GB76MIDL07009363082894','GB47MIDL07009340930750', 'GBP', '300.0','debt');