package com.simple.banking;

import com.jayway.restassured.response.Header;
import com.simple.banking.config.BankingConfiguration;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit5.DropwizardAppExtension;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.UUID;

@ExtendWith({DropwizardExtensionsSupport.class})
public class BankingIntegrationTest {

    protected static final String CONFIG_PATH = ResourceHelpers.resourceFilePath("test-config.yml");

    protected static final DropwizardAppExtension<BankingConfiguration> application =
            new DropwizardAppExtension<BankingConfiguration>(BankingApplication.class, CONFIG_PATH);

    protected UUID USER_1_UUID = UUID.fromString("6dc7a8c0-4de6-11ea-b77f-2e728ce88125");
    protected UUID ACCOUNT_1_UUID = UUID.fromString("8c2105f0-4de6-11ea-b77f-2e728ce88125");
    protected UUID ACCOUNT_2_UUID = UUID.fromString("e534b384-4de7-11ea-b77f-2e728ce88125");
    protected UUID CURRENCY_1_UUID = UUID.fromString("8c21037a-4de6-11ea-b77f-2e728ce88125");
    protected UUID TRANSACTION_1_UUID = UUID.fromString("6dc7a8c0-4de6-11ea-b77f-2e728ce88125");

    protected Header getHeader() {
        return new Header("Content-Type", "application/json");
    }
}
