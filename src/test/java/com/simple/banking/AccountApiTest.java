package com.simple.banking;


import com.simple.banking.domain.enums.Currency;
import com.simple.banking.dto.CreateAccountDTO;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.jayway.restassured.RestAssured.given;
import static com.simple.banking.util.MyStringUtils.toJson;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

@ExtendWith({DropwizardExtensionsSupport.class})
public class AccountApiTest extends BankingIntegrationTest {

    private static String baseUrl = "http://localhost:8083/api/account/";

    @Test
    public void createAccount_ok() {
        CreateAccountDTO createAccountDTO = CreateAccountDTO.builder()
                .accountNumber("63182124")
                .branchId(123)
                .currency(Currency.USD)
                .iban("GB76MIDL07009363082843")
                .userId(USER_1_UUID)
                .build();

        given()
                .body(toJson(createAccountDTO))
                .header(getHeader())
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("accountNumber", equalTo(createAccountDTO.getAccountNumber()))
                .body("iban", equalTo(createAccountDTO.getIban()))
                .body("branchId", equalTo(createAccountDTO.getBranchId()))
                .body("currency", equalTo(createAccountDTO.getCurrency().toString()))
                .log();
    }

    @Test
    public void getAccount_ok() {

        given()
                .header(getHeader())
                .when()
                .get(baseUrl + ACCOUNT_1_UUID).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("accountNumber", equalTo("63082894"))
                .body("branchId", equalTo(101))
                .body("iban", equalTo("GB76MIDL07009363082894"))
                .body("currency", equalTo("EUR"))
                .log();
    }

    @Test
    public void createAccountWithMissingParameter() {
        CreateAccountDTO createAccountDTO = CreateAccountDTO.builder()
                .branchId(123)
                .currency(Currency.USD)
                .iban("GB76MIDL07009363082843")
                .userId(USER_1_UUID)
                .build();

        given()
                .body(toJson(createAccountDTO))
                .header(getHeader())
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(422)
                .log();
    }
}
