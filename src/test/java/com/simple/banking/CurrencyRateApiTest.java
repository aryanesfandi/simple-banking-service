package com.simple.banking;


import com.simple.banking.domain.enums.Currency;
import com.simple.banking.dto.CreateCurrencyRateDTO;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.jayway.restassured.RestAssured.given;
import static com.simple.banking.util.MyStringUtils.toJson;
import static org.hamcrest.CoreMatchers.*;

@ExtendWith({DropwizardExtensionsSupport.class})
public class CurrencyRateApiTest extends BankingIntegrationTest {

    private static String baseUrl = "http://localhost:8083/api/currency/";

    @Test
    public void createCurrencyRate_ok() {
        CreateCurrencyRateDTO createCurrencyRateDTO = CreateCurrencyRateDTO.builder()
                .source(Currency.EUR)
                .destination(Currency.USD)
                .rate(1.09)
                .build();

        given()
                .body(toJson(createCurrencyRateDTO))
                .header(getHeader())
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("source", equalTo(createCurrencyRateDTO.getSource().toString()))
                .body("destination", equalTo(createCurrencyRateDTO.getDestination().toString()))
                .body("rate", equalTo(createCurrencyRateDTO.getRate().floatValue()))
                .log();

    }

    @Test
    public void getCurrencyRate_ok() {
        given()
                .header(getHeader())
                .when()
                .get(baseUrl + CURRENCY_1_UUID).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("source", equalTo("EUR"))
                .body("destination", equalTo("GBP"))
                .body("rate", is(Double.valueOf(0.84).floatValue()))
                .log();
    }

    @Test
    public void createCurrencyRateWithMissingParameter() {
        CreateCurrencyRateDTO createCurrencyRateDTO = CreateCurrencyRateDTO.builder()
                .source(Currency.EUR)
                .rate(1.09)
                .build();

        given()
                .body(toJson(createCurrencyRateDTO))
                .header(getHeader())
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(422)
                .log();
    }
}
