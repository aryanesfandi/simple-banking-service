package com.simple.banking;


import com.jayway.restassured.response.Header;
import com.simple.banking.domain.enums.UserType;
import com.simple.banking.dto.CreateUserDTO;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import static com.jayway.restassured.RestAssured.given;
import static com.simple.banking.util.MyStringUtils.toJson;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

@ExtendWith({DropwizardExtensionsSupport.class})
public class UserApiTest extends BankingIntegrationTest {

    private static String baseUrl = "http://localhost:8083/api/user/";

    @Test
    public void createUser_ok() {
        CreateUserDTO createUserDTO = CreateUserDTO.builder()
                .userType(UserType.PERSON)
                .firstName("ali")
                .lastName("zia")
                .email("test@gmail.com")
                .build();

        given()
                .body(toJson(createUserDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("firstName", equalTo(createUserDTO.getFirstName()))
                .body("lastName", equalTo(createUserDTO.getLastName()))
                .body("email", equalTo(createUserDTO.getEmail()))
                .log();

    }

    @Test
    public void getUser_ok() {

        given()
                .header(new Header("Content-Type", "application/json"))
                .when()
                .get(baseUrl + USER_1_UUID).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("firstName", equalTo("aryan"))
                .body("lastName", equalTo("esfandi kashi"))
                .body("email", equalTo("aryan@gmail.com"))
                .log();

    }

    @Test
    public void createUserWithMissingParameters() {
        CreateUserDTO createUserDTO = CreateUserDTO.builder()
                .firstName("ali")
                .lastName("zia")
                .email("ali@gmail.com")
                .build();

        given()
                .body(toJson(createUserDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(422)
                .extract().asString();
    }
}
