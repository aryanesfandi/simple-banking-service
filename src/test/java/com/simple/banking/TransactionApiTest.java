package com.simple.banking;


import com.jayway.restassured.response.Header;
import com.simple.banking.domain.enums.Currency;
import com.simple.banking.domain.enums.UserType;
import com.simple.banking.dto.CreateAccountDTO;
import com.simple.banking.dto.CreateTransactionDTO;
import io.dropwizard.testing.junit5.DropwizardExtensionsSupport;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.math.BigDecimal;

import static com.jayway.restassured.RestAssured.given;
import static com.simple.banking.util.MyStringUtils.toJson;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

@ExtendWith({DropwizardExtensionsSupport.class})
public class TransactionApiTest extends BankingIntegrationTest {

    private static String baseUrl = "http://localhost:8083/api/transaction/";
    private static String accountUrl = "http://localhost:8083/api/account/";

    @Test
    public void createTransaction_ok() {
        CreateTransactionDTO createTransactionDTO = CreateTransactionDTO.builder()
                .sourceIban("GB76MIDL07009363082894")
                .destinationIban("GB47MIDL07009340930750")
                .amount(BigDecimal.valueOf(300.0))
                .destinationUserType(UserType.PERSON)
                .destinationFirstName("iman")
                .destinationLastName("yarandi")
                .description("gift")
                .build();

        given()
                .body(toJson(createTransactionDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("id", notNullValue())
                .body("sourceIban", equalTo("GB76MIDL07009363082894"))
                .body("destinationIban", equalTo("GB47MIDL07009340930750"))
                .body("currency", equalTo("GBP"))
                .body("amount", equalTo(Float.valueOf("252.0")))
                .body("description", equalTo("gift"))
                .log();

        given()
                .header(new Header("Content-Type", "application/json"))
                .when()
                .get(accountUrl + ACCOUNT_1_UUID).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("balance", equalTo(Double.valueOf(200).floatValue()))
                .log();

        given()
                .header(new Header("Content-Type", "application/json"))
                .when()
                .get(accountUrl + ACCOUNT_2_UUID).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("balance", equalTo(Double.valueOf(252.0).floatValue()))
                .log();
    }

    @Test
    public void createTransactionWithWrongIdentificationData() {
        CreateTransactionDTO createTransactionDTO = CreateTransactionDTO.builder()
                .sourceIban("GB76MIDL07009363082894")
                .destinationIban("GB47MIDL07009340930750")
                .amount(BigDecimal.valueOf(300.0))
                .destinationUserType(UserType.PERSON)
                .destinationFirstName("iman")
                .destinationLastName("esfandi")
                .description("gift")
                .build();

        given()
                .body(toJson(createTransactionDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(311)
                .log();
    }

    @Test
    public void createTransactionWithInsufficientBalance() {
        CreateTransactionDTO createTransactionDTO = CreateTransactionDTO.builder()
                .sourceIban("GB76MIDL07009363082894")
                .destinationIban("GB47MIDL07009340930750")
                .amount(BigDecimal.valueOf(1500.0))
                .destinationUserType(UserType.PERSON)
                .destinationFirstName("iman")
                .destinationLastName("yarandi")
                .description("gift")
                .build();

        given()
                .body(toJson(createTransactionDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(312)
                .log();
    }


    @Test
    public void createTransactionWithLessThanMinimumAmount() {
        CreateTransactionDTO createTransactionDTO = CreateTransactionDTO.builder()
                .sourceIban("GB76MIDL07009363082894")
                .destinationIban("GB47MIDL07009340930750")
                .amount(BigDecimal.valueOf(0.10))
                .destinationUserType(UserType.PERSON)
                .destinationFirstName("iman")
                .destinationLastName("yarandi")
                .description("gift")
                .build();

        given()
                .body(toJson(createTransactionDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(314)
                .log();
    }

    @Test
    public void createTransactionWithNotFoundConverter() {
        CreateTransactionDTO createTransactionDTO = CreateTransactionDTO.builder()
                .sourceIban("GB47MIDL07009340930750")
                .destinationIban("GB76MIDL07009363082894")
                .amount(BigDecimal.valueOf(300.0))
                .destinationUserType(UserType.PERSON)
                .destinationFirstName("aryan")
                .destinationLastName("esfandi kashi")
                .description("debt")
                .build();

        given()
                .body(toJson(createTransactionDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(315)
                .log();
    }

    @Test
    public void createTransactionWithWrongDestinationIban() {
        CreateTransactionDTO createTransactionDTO = CreateTransactionDTO.builder()
                .sourceIban("GB76MIDL07009363082894")
                .destinationIban("GB47MIDL07009340930759")
                .amount(BigDecimal.valueOf(300.0))
                .destinationUserType(UserType.PERSON)
                .destinationFirstName("iman")
                .destinationLastName("yarandi")
                .description("gift")
                .build();

        given()
                .body(toJson(createTransactionDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(404)
                .log();
    }


    @Test
    public void getTransaction_ok() {
        given()
                .header(new Header("Content-Type", "application/json"))
                .when()
                .get(baseUrl + TRANSACTION_1_UUID).prettyPeek()
                .then()
                .statusCode(200)
                .body("id", notNullValue())
                .body("sourceIban", equalTo("GB76MIDL07009363082894"))
                .body("destinationIban", equalTo("GB47MIDL07009340930750"))
                .body("currency", equalTo("GBP"))
                .body("amount", equalTo(Float.valueOf("300.0")))
                .body("description", equalTo("debt"))
                .log();

    }

    @Test
    public void createAccountWithMissingParameter() {
        CreateAccountDTO createAccountDTO = CreateAccountDTO.builder()
                .branchId(123)
                .currency(Currency.USD)
                .iban("GB76MIDL07009363082843")
                .userId(USER_1_UUID)
                .build();

        given()
                .body(toJson(createAccountDTO))
                .header(new Header("Content-Type", "application/json"))
                .when()
                .post(baseUrl).prettyPeek()
                .then()
                .statusCode(422)
                .log();

    }
}
