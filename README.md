# simple-banking-service
*This is a simple api to provide simple money transfer*

I used [Dropwizard] as main framework, [Google Guice] as DI, [Hibernate] as ORM,[Postgres] as Database, Flyway as database migration, [Rest assured] and [Unit test] for Testing purpose and [Maven] as Package manager.

This service runs Embedded Postgres if you want to use your own database just change a config.
For creating jar file you need to run 

`mvn package `
 
To run the service

`java -jar target/banking-1.0.0.jar server config.yml`
